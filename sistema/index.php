<?php
	@session_start();
    require_once('../classes/Login.php');

    if (Login::isLogado() == false && Login::isAdmin() == false) {
        require_once('login.phtml');
        exit;
    }

	require_once('../classes/Core.php');

	if(Core::getRequest('msg')){
		$msg = Core::getRequest('msg');
	}else{
		$msg = '';
	}
?>
<html>
<head>
<?php require_once('head.php'); ?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
</head>

<body>
	<div class="sistema">
		<div class="col-left">
			<?php require_once('col-left.php'); ?>
		</div>
		<div class="col-main">
			<?php require_once('header.php'); ?>
			<div class="">
				<?php require_once('dashboard.php'); ?>
				<p class="clear"></p>
			</div>
		</div>
		<p class="clear"></p>
	</div>
</body>
</html>
