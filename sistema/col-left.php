<?php
	require_once('../classes/Login.php');
?>

<div class="col-welcome">
	<div class="profile-icon">
		<a href="profile.php">
			<img src="images/icon-welcome.png" class="profile-img img-circle" alt="Minha Conta" title="Minha Conta" />
		</a>
	</div>
	<div class="welcome-msg">
		<span>Seja bem-vindo,</span>
		<h2><?php echo Login::getNomeUsuario(); ?></h2>
	</div>
	<a href="javascript:void(0);" class="icon menu-mobile-icon" onclick="myFunction()">
		<i class="fa fa-bars"></i>
	</a>
	<p class="clear"></p>
</div>
<div id="sidebar-menu" class="main-menu-side main-menu">
	<div class="menu-section" id="menu-section">
		<?php if (Login::isAdmin()): ?>
			<h3>- Menu Inicial</h3>
			<ul class="nav side-menu">
				<li>
					<a href="clientes.php">Clientes</a>
				</li>
				<li>
					<a href="produtos.php">Produtos</a>
				</li>
				<li>
					<a href="vendas.php">Vendas</a>
				</li>
			</ul>
		<?php endif; ?>
	</div>
</div>