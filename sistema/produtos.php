<?php
	@session_start();
    require_once('../classes/Login.php');

    if (Login::isLogado() == false && Login::isAdmin() == false) {
        require_once('login.phtml');
        exit;
    }
	if (!Login::isAdmin()){
		echo "<meta http-equiv='refresh' content='0;URL=index.php'>";
		exit;
	}


	require_once('../classes/Core.php');
	require_once('../classes/Data.php');
	require_once('../classes/Valor.php');

	
	require_once("../mod/produtos/classes/Produto.php");
	require_once("../mod/produtos/classes/ProdutoDAO.php");

	if(Core::getRequest('msg')){
		$msg = Core::getRequest('msg');
	}else{
		$msg = '';
	}

	if(Core::getPost('nome')){
		$nome = Core::getPost('nome');
	}elseif(Core::getRequest('nome')){
		$nome = Core::getRequest('nome');
	}else{
		$nome = '';
	}

	if(Core::getRequest('id')){
		$id = Core::getRequest('id');
		$action = Core::getRequest('action');
		if($action == 'delete'){
			if(Login::isAdmin())
				$msg = ProdutoDAO::delete($id);
		}
	}

	$maxProdutos = 20; //numero máximo de resultados por página
	if(array_key_exists('qtde', $_REQUEST)){
		$maxProdutos = Core::getRequest('qtde');
	}

	$pagina = 1; //pagina atual
	if (array_key_exists('pagina', $_REQUEST)) {
		$pagina = Core::getRequest('pagina');
	}

	$produtos = ProdutoDAO::getProdutos($maxProdutos, $pagina, $nome);
	$numRegistros = ProdutoDAO::getNumRegEncontrados();
	if ($numRegistros > 0) {
		$numPaginas = ceil($numRegistros / $maxProdutos);
	}
	else {
		$numPaginas = 0;
	}

?>
<html>
<head>
<?php require_once('head.php'); ?>
<script type="text/javascript">

</script>
</head>

<body>
	<div class="sistema">
		<div class="col-left">
			<?php require_once('col-left.php'); ?>
		</div>
		<div class="col-main">
			<?php require_once('header.php'); ?>
			<div class="block-default usuarios">
				<div class="block-content">
					<div class="block-title">
						<div class="title">
							<span>Produtos</span>
						</div>
						<div class="buttons-right">
							<?php if(Login::isAdmin()): ?>
								<a href="produto-adicionar.php" class="btn-link btn-add">
									<i class="fas fa-plus"></i>
									Adicionar Produto
								</a>
						<?php endif; ?>
						</div>
					</div>
					<div class="block-title filtros">
						<div class="title">
							<span>Filtros: </span>
						</div>
						<div class="filtro">
							<form name="formulario" method="POST" action="<?php $_SERVER['PHP_SELF']; ?>" >
								<div>
									<span>
										<label>Nome:</label>
										<input type="text" name="nome" id="nome" class="campo" value="<?php echo $nome; ?>" />
									</span>
								</div>
								<p>
									<input type="submit" name="enviar" id="enviar" value="FILTRAR" class="botao" />
									<?php if($nome){ ?>
										<a href="<?php $_SERVER['PHP_SELF']; ?>" class="botao">LIMPAR FILTROS</a>
									<?php } ?>
								</p>
							</form>
						</div>
					</div>
					<div class="msg"><?php echo $msg; ?></div>
					<div class="">
						<div class="lista-pager">
							<form name="paginacao" method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
								<table cellpadding="0" cellspacing="0" border="0">
									<tbody>
										<tr>
											<td class="left">
												<span class="pagina">Página</span>
												<?php
													if($pagina - 1 >= 1){
														$vPagina = $pagina - 1;
														echo '<span class="pagina"><a href="'.$_SERVER['PHP_SELF'].'?pagina='.$vPagina.'&qtde='.$maxProdutos.'&nome=' . $nome . '"><i class="fas fa-angle-double-left"></i></span></a>';
													}
												?>
												<input type="text" name="pagina" id="pagina" value="<?php echo $pagina; ?>" class="campo" />
												<?php
													if($pagina + 1 <= $numPaginas){
														$pPagina = $pagina + 1;
														echo '<span class="pagina"><a href="'.$_SERVER['PHP_SELF'].'?pagina='.$pPagina.'&qtde='.$maxProdutos.'&nome=' . $nome . '"><i class="fas fa-angle-double-right"></i></span></a>';
													}
												?>
												<span class="pagina">de <?php echo $numPaginas; ?> Página(s)</span>
												<p class="clear"></p>
											</td>
										</tr>
									</tbody>
								</table>
							</form>
						</div>
					</div>
					<div class="">
						<form name="acao" method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
							<div class="lista-default">
								<table cellpadding="0" cellspacing="0" border="0">
									<thead>
										<tr>
											<th class="status">SKU</th>
											<th class="nome">Nome</th>
											<th class="status">Valor Un.</th>
											<th class="status">Ação</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$i = 1;
											foreach($produtos as $produto){
										?>
											<tr class="<?php if($i==1){echo "pintaLista"; $i=0;} else $i=1; ?>">
												<td class="first status"><?php echo $produto->sku; ?></td>
												<td class="nome"><?php echo $produto->nome; ?></td>
												<td class="status"><?php echo Valor::sqlToBr($produto->valor); ?> </td>
												<td class="status <?php echo $classe; ?>">
													<?php if(Login::isAdmin()): ?>
													<a href="produto-adicionar.php?id=<?php echo $produto->id; ?>">
														<i class="fas fa-pencil-alt"></i>
													</a>
													&nbsp;&nbsp;
													<a href="produtos.php?id=<?php echo $produto->id; ?>&action=delete" onclick="return confirm('Deseja excluir o produto <?php echo $produto->nome; ?>?')">
														<i class="fas fa-trash-alt"></i>
													</a>
													&nbsp;&nbsp;
													<?php endif; ?>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</form>
					</div>
					<div class="lista-default">

					</div>
				</div>
			</div>

		</div>
		<?php //require_once('footer.php'); ?>
	</div>
</body>
</html>
