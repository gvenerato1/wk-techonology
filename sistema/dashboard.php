<?php
	@session_start();
    require_once('../classes/Login.php');
    if (Login::isLogado() == false && Login::isAdmin() == false) {
        require_once('login.phtml');
        exit;
    }
?>
<div>
	<div class="block-default home">
		<div class="block-content dashboard">
			<h1>WK Technology</h1>
		</div>
	</div>
</div>