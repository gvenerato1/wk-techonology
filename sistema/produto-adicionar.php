<?php
	@session_start();
    require_once('../classes/Login.php');

    if (Login::isLogado() == false && Login::isAdmin() == false) {
        require_once('login.phtml');
        exit;
    }
	if (!Login::isAdmin()){
		echo "<meta http-equiv='refresh' content='0;URL=index.php'>";
		exit;
	}

	require_once('../classes/Core.php');
	require_once('../classes/Data.php');
	require_once('../classes/Valor.php');
	require_once("../mod/produtos/classes/Produto.php");
	require_once("../mod/produtos/classes/ProdutoDAO.php");

	$id = Core::getRequest('id');
	if(!empty($id) || $id > 0){
		$produto = ProdutoDAO::getProduto($id);
	}else{
		$produto = new Produto();
	}
?>
<html>
<head>
<?php require_once('head.php'); ?>
</head>

<body>
	<div class="sistema">
		<div class="col-left">
			<?php require_once('col-left.php'); ?>
		</div>
		<div class="col-main">
			<?php require_once('header.php'); ?>
			<div class="block-default usuarios">
				<div class="block-content">
					<div class="block-title">
						<div class="title">
							<span><?php if($id) echo "Atualizar"; else echo "Novo"; ?> Produto</span>
						</div>
					</div>
					<div class="block-form">
						<form name="formulario" action="../mod/produtos/services/grava.php" method="POST" enctype="multipart/form-data" autocomplete="off">
							<input type="hidden" name="id" id="id" value="<?php echo $produto->id; ?>" />
							<div class="form-group">
								<label>SKU *</label>
								<input type="text" name="sku" id="sku" value="<?php echo $produto->sku; ?>" class="campo required" />
							</div>
							
							<div class="form-group">
								<label>Nome *</label>
								<input type="text" name="nome" id="nome" value="<?php echo $produto->nome; ?>" class="campo required" />
							</div>

							<div class="form-group">
								<label>Valor *</label>
								<input type="text" name="valor" id="valor" value="<?php echo Valor::sqlToBrInput($produto->valor); ?>" class="campo required" />
							</div>

							<div class="form-group">
								<label>Status</label>
								<select name="status" id="status" class="campo ">
									<option value="1" <?php if($produto->status == 1){ echo 'selected'; } ?>>Habilitado</option>
									<option value="0" <?php if($produto->status == 0){ echo 'selected'; } ?>>Desabilitado</option>
								</select>
							</div>
							<div class="bottom-actions">
								<button type="send" class="btn-link"><i class="fas fa-check"></i> Salvar</button>
								<a href="produto-adicionar.php" class="btn-link"><i class="fas fa-undo-alt"></i> Limpar</a>
								<a href="produtos.php" class="btn-link btn-voltar"><i class="fas fa-arrow-left"></i> Voltar</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
