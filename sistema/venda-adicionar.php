<?php
	@session_start();
    require_once('../classes/Login.php');

    if (Login::isLogado() == false && Login::isAdmin() == false) {
        require_once('login.phtml');
        exit;
    }
	if (!Login::isAdmin()){
		echo "<meta http-equiv='refresh' content='0;URL=index.php'>";
		exit;
	}

	require_once('../classes/Core.php');
	require_once('../classes/Data.php');
	require_once('../classes/Valor.php');
	require_once("../mod/vendas/classes/Venda.php");
	require_once("../mod/vendas/classes/VendaDAO.php");
	require_once("../mod/clientes/classes/ClienteDAO.php");
	require_once("../mod/produtos/classes/ProdutoDAO.php");

	$id = Core::getRequest('id');
	if(!empty($id) || $id > 0){
		$venda = VendaDAO::getVenda($id);
	}else{
		$venda = new Venda();
	}
	$clientes = ClienteDAO::getClientes();
	$produtos = ProdutoDAO::getProdutos();
?>
<html>
<head>
<?php require_once('head.php'); ?>
</head>

<body>
	<div class="sistema">
		<div class="col-left">
			<?php require_once('col-left.php'); ?>
		</div>
		<div class="col-main">
			<?php require_once('header.php'); ?>
			<div class="block-default usuarios">
				<div class="block-content">
					<div class="block-title">
						<div class="title">
							<span><?php if($id) echo "Atualizar"; else echo "Nova"; ?> Venda</span>
						</div>
					</div>
					<div class="block-form">
						<form name="formulario" action="../mod/vendas/services/grava.php" method="POST" enctype="multipart/form-data" autocomplete="off">
							<input type="hidden" name="id" id="id" value="<?php echo $venda->id; ?>" />
							<div class="form-group">
								<label>Cliente *</label>
								<select name="idCliente" id="idCliente" class="campo required">
									<option>Selecione o cliente...</option>
									<?php foreach($clientes as $cliente){ ?>
										<option value="<?php echo $cliente->id; ?>" <?php if($cliente->id == $venda->idCliente): echo 'selected'; endif; ?>><?php echo $cliente->nome; ?></option>
									<?php } ?>
								</select>
							</div>
							
							<div class="form-products">
								<div class="form-group">
									<label>Produto *</label>
									<select name="idProduto[]" id="idProduto" class="campo required">
										<option>Selecione o produto...</option>
										<?php foreach($produtos as $produto){ ?>
											<option value="<?php echo $produto->id; ?>"><?php echo $produto->nome; ?></option>
										<?php } ?>
									</select>
									<a href="#" class="more-products">+ MAIS PRODUTOS</a>
								</div>
							</div>

							<div class="form-group">
								<label>Status</label>
								<select name="status" id="status" class="campo ">
									<option value="1" <?php if($venda->status == 1){ echo 'selected'; } ?>>Habilitado</option>
									<option value="0" <?php if($venda->status == 0){ echo 'selected'; } ?>>Desabilitado</option>
								</select>
							</div>
							<div class="bottom-actions">
								<button type="send" class="btn-link"><i class="fas fa-check"></i> Salvar</button>
								<a href="venda-adicionar.php" class="btn-link"><i class="fas fa-undo-alt"></i> Limpar</a>
								<a href="vendas.php" class="btn-link btn-voltar"><i class="fas fa-arrow-left"></i> Voltar</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			var regex = /^(.+?)(\d+)$/i;
			var cloneIndex = $(".clonedInput").length + 1;

			function clone(){
				var formClone = '<div class="form-group"><label>Produto *</label>';
				formClone += '<select name="idProduto[]" id="idProduto" class="campo required">';
				formClone += '<option>Selecione o produto...</option>';
				<?php foreach($produtos as $produto){ ?>
					formClone += '<option value="<?php echo $produto->id; ?>"><?php echo $produto->nome; ?></option>';
				<?php } ?>
				formClone += '</select>';
				formClone += '<a href="#" class="more-products">+ MAIS PRODUTOS</a>';
				formClone += '</div>';
				$('.form-products').append(formClone);
				cloneIndex++;
			}
			$("a.more-products").on("click", function(event){
				event.preventDefault()
				clone();
			});
		</script>
	</div>
</body>
</html>
