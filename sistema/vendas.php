<?php
	@session_start();
    require_once('../classes/Login.php');

    if (Login::isLogado() == false && Login::isAdmin() == false) {
        require_once('login.phtml');
        exit;
    }
	if (!Login::isAdmin()){
		echo "<meta http-equiv='refresh' content='0;URL=index.php'>";
		exit;
	}


	require_once('../classes/Core.php');
	require_once('../classes/Data.php');
	require_once('../classes/Valor.php');

	
	require_once("../mod/vendas/classes/Venda.php");
	require_once("../mod/vendas/classes/VendaDAO.php");
	require_once("../mod/clientes/classes/ClienteDAO.php");
	require_once("../mod/produtos/classes/ProdutoDAO.php");

	if(Core::getRequest('msg')){
		$msg = Core::getRequest('msg');
	}else{
		$msg = '';
	}

	if(Core::getPost('nome')){
		$nome = Core::getPost('nome');
	}elseif(Core::getRequest('nome')){
		$nome = Core::getRequest('nome');
	}else{
		$nome = '';
	}

	if(Core::getRequest('id')){
		$id = Core::getRequest('id');
		$action = Core::getRequest('action');
		if($action == 'delete'){
			if(Login::isAdmin())
				$msg = VendaDAO::delete($id);
		}
	}

	$maxVendas = 20; //numero máximo de resultados por página
	if(array_key_exists('qtde', $_REQUEST)){
		$maxVendas = Core::getRequest('qtde');
	}

	$pagina = 1; //pagina atual
	if (array_key_exists('pagina', $_REQUEST)) {
		$pagina = Core::getRequest('pagina');
	}

	$vendas = VendaDAO::getVendas($maxVendas, $pagina, $nome);
	$numRegistros = VendaDAO::getNumRegEncontrados();
	if ($numRegistros > 0) {
		$numPaginas = ceil($numRegistros / $maxVendas);
	}
	else {
		$numPaginas = 0;
	}

?>
<html>
<head>
<?php require_once('head.php'); ?>
<script type="text/javascript">

</script>
</head>

<body>
	<div class="sistema">
		<div class="col-left">
			<?php require_once('col-left.php'); ?>
		</div>
		<div class="col-main">
			<?php require_once('header.php'); ?>
			<div class="block-default usuarios">
				<div class="block-content">
					<div class="block-title">
						<div class="title">
							<span>Vendas</span>
						</div>
						<div class="buttons-right">
							<?php if(Login::isAdmin()): ?>
								<a href="venda-adicionar.php" class="btn-link btn-add">
									<i class="fas fa-plus"></i>
									Adicionar Venda
								</a>
						<?php endif; ?>
						</div>
					</div>
					<div class="block-title filtros">
						<div class="title">
							<span>Filtros: </span>
						</div>
						<div class="filtro">
							<form name="formulario" method="POST" action="<?php $_SERVER['PHP_SELF']; ?>" >
								<div>
									<span>
										<label>Nome:</label>
										<input type="text" name="nome" id="nome" class="campo" value="<?php echo $nome; ?>" />
									</span>
								</div>
								<p>
									<input type="submit" name="enviar" id="enviar" value="FILTRAR" class="botao" />
									<?php if($nome){ ?>
										<a href="<?php $_SERVER['PHP_SELF']; ?>" class="botao">LIMPAR FILTROS</a>
									<?php } ?>
								</p>
							</form>
						</div>
					</div>
					<div class="msg"><?php echo $msg; ?></div>
					<div class="">
						<div class="lista-pager">
							<form name="paginacao" method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
								<table cellpadding="0" cellspacing="0" border="0">
									<tbody>
										<tr>
											<td class="left">
												<span class="pagina">Página</span>
												<?php
													if($pagina - 1 >= 1){
														$vPagina = $pagina - 1;
														echo '<span class="pagina"><a href="'.$_SERVER['PHP_SELF'].'?pagina='.$vPagina.'&qtde='.$maxVendas.'&nome=' . $nome . '"><i class="fas fa-angle-double-left"></i></span></a>';
													}
												?>
												<input type="text" name="pagina" id="pagina" value="<?php echo $pagina; ?>" class="campo" />
												<?php
													if($pagina + 1 <= $numPaginas){
														$pPagina = $pagina + 1;
														echo '<span class="pagina"><a href="'.$_SERVER['PHP_SELF'].'?pagina='.$pPagina.'&qtde='.$maxVendas.'&nome=' . $nome . '"><i class="fas fa-angle-double-right"></i></span></a>';
													}
												?>
												<span class="pagina">de <?php echo $numPaginas; ?> Página(s)</span>
												<p class="clear"></p>
											</td>
										</tr>
									</tbody>
								</table>
							</form>
						</div>
					</div>
					<div class="">
						<form name="acao" method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
							<div class="lista-default">
								<table cellpadding="0" cellspacing="0" border="0">
									<thead>
										<tr>
											<th class="status">ID</th>
											<th class="status">Data</th>
											<th class="nome">Cliente</th>
											<th class="nome">Produtos</th>
											<th class="status">Total da Venda</th>
											<th class="status">Ação</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$i = 1;
											foreach($vendas as $venda){
												$produtosVenda = VendaDAO::getProdutosVenda($venda->id);
										?>
											<tr class="<?php if($i==1){echo "pintaLista"; $i=0;} else $i=1; ?>">
												<td class="first status"><?php echo $venda->id; ?></td>
												<td class="status"><?php echo Data::sqlToBr($venda->dataVenda); ?></td>
												<td class="nome">
													<?php 
														$cliente = ClienteDAO::getCliente($venda->idCliente); 
														echo $cliente->nome . ' ' . $cliente->sobrenome;
													?>
												</td>
												<td class="nome">
													<?php 
														foreach($produtosVenda as $produtoVenda){ 
															echo '<p>' . ProdutoDAO::getProduto($produtoVenda->idProduto)->nome . '</p>';
														}
													?>
												</td>
												<td class="status"><?php echo Valor::sqlToBr($venda->total); ?></td>
												<td class="status <?php echo $classe; ?>">
													<?php if(Login::isAdmin()): ?>
													<a href="venda-adicionar.php?id=<?php echo $venda->id; ?>">
														<i class="fas fa-pencil-alt"></i>
													</a>
													&nbsp;&nbsp;
													<a href="vendas.php?id=<?php echo $venda->id; ?>&action=delete" onclick="return confirm('Deseja excluir a venda <?php echo $venda->id; ?>?')">
														<i class="fas fa-trash-alt"></i>
													</a>
													&nbsp;&nbsp;
													<?php endif; ?>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</form>
					</div>
					<div class="lista-default">

					</div>
				</div>
			</div>

		</div>
		<?php //require_once('footer.php'); ?>
	</div>
</body>
</html>
