<?php
	@session_start();
    require_once('../classes/Login.php');

    if (Login::isLogado() == false && Login::isAdmin() == false) {
        require_once('login.phtml');
        exit;
    }
	if (!Login::isAdmin()){
		echo "<meta http-equiv='refresh' content='0;URL=index.php'>";
		exit;
	}

	require_once('../classes/Core.php');
	require_once('../classes/Data.php');
	require_once("../mod/clientes/classes/Cliente.php");
	require_once("../mod/clientes/classes/ClienteDAO.php");

	$id = Core::getRequest('id');
	if(!empty($id) || $id > 0){
		$cliente = ClienteDAO::getCliente($id);
	}else{
		$cliente = new Cliente();
	}
?>
<html>
<head>
<?php require_once('head.php'); ?>
</head>

<body>
	<div class="sistema">
		<div class="col-left">
			<?php require_once('col-left.php'); ?>
		</div>
		<div class="col-main">
			<?php require_once('header.php'); ?>
			<div class="block-default usuarios">
				<div class="block-content">
					<div class="block-title">
						<div class="title">
							<span><?php if($id) echo "Atualizar"; else echo "Novo"; ?> Cliente</span>
						</div>
					</div>
					<div class="block-form">
						<form name="formulario" action="../mod/clientes/services/grava.php" method="POST" enctype="multipart/form-data" autocomplete="off">
							<input type="hidden" name="id" id="id" value="<?php echo $cliente->id; ?>" />
							<div class="form-group">
								<label>Nome *</label>
								<input type="text" name="nome" id="nome" value="<?php echo $cliente->nome; ?>" class="campo required" />
							</div>

							<div class="form-group">
								<label>Sobrenome</label>
								<input type="text" name="sobrenome" id="sobrenome" value="<?php echo $cliente->sobrenome; ?>" class="campo" />
							</div>
							
							<div class="form-group">
								<label>CPF *</label>
								<input type="text" name="cpf" id="cpf" value="<?php echo $cliente->cpf; ?>" class="campo required" />
							</div>
							
							<div class="form-group">
								<label>Endereço</label>
								<input type="text" name="endereco" id="endereco" value="<?php echo $cliente->endereco; ?>" class="campo" />
							</div>

							<div class="form-group">
								<label>E-mail</label>
								<input type="text" name="email" id="email" value="<?php echo $cliente->email; ?>" class="campo" />
							</div>

							<div class="form-group">
								<label>Data de nascimento</label>
								<input type="text" name="nascimento" id="nascimento" value="<?php echo Data::sqlToBr($cliente->nascimento); ?>" class="campo" />
							</div>

							<div class="form-group">
								<label>Status</label>
								<select name="status" id="status" class="campo ">
									<option value="1" <?php if($cliente->status == 1){ echo 'selected'; } ?>>Habilitado</option>
									<option value="0" <?php if($cliente->status == 0){ echo 'selected'; } ?>>Desabilitado</option>
								</select>
							</div>
							<div class="bottom-actions">
								<button type="send" class="btn-link"><i class="fas fa-check"></i> Salvar</button>
								<a href="cliente-adicionar.php" class="btn-link"><i class="fas fa-undo-alt"></i> Limpar</a>
								<a href="clientes.php" class="btn-link btn-voltar"><i class="fas fa-arrow-left"></i> Voltar</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
