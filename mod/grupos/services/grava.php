<?php
	require_once(dirname(__FILE__) . '/../../../classes/config.php');
	require_once(dirname(__FILE__) . '/../../../classes/injecao.php');
	require_once(dirname(__FILE__) . '/../../../classes/Core.php');
	require_once(dirname(__FILE__) . '/../../../classes/Data.php');
	require_once(dirname(__FILE__) . '/../../../classes/Upload.php');
	require_once(dirname(__FILE__) . '/../../../classes/Texto.php');
	require_once(dirname(__FILE__) . '/../../../classes/Senha.php');
	require_once("../classes/Grupo.php");
?>

<?php

	$grupo = new Grupo();
	$grupo->id = limpaSQL(strip_tags(trim(Core::getPost('id'))));
	$grupo->nome = limpaSQL(strip_tags(trim(Core::getPost('nome'))));
	$grupo->codigo = limpaSQL(strip_tags(trim(Core::getPost('codigo'))));
	$grupo->status = Core::getPost('status');
	if(!empty($grupo->nome) && !empty($grupo->codigo)){
		$id = $grupo->save();
		if($id > 0){
			echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/grupos.php?msg=Operação realizada com sucesso!'>";
		}
		elseif($id == -1){
			echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/grupos.php?msg=Ocorreu um erro, por favor tente novamente!'>";
		}
	}
	else{
		echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/grupos.php?msg=Preencha os campos obrigatórios!'>";
	}
?>
