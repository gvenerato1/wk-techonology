<?php
	class Grupo{
		//declaracao das variaveis
		public $id = 0;
		public $nome = '';
		public $codigo = '';
		public $status = 1;

		function save(){
			require_once('GrupoDAO.php');
			return(GrupoDAO::save($this));
		}

		function getPermissao($crud, $opcao)
		{
			$opcoes = explode(',',$crud);
			if($opcao=='c')
				return $opcoes[0];
			if($opcao=='r')
				return $opcoes[1];
			if($opcao=='u')
				return $opcoes[2];
			if($opcao=='d')
				return $opcoes[3];
		}

	}
?>
