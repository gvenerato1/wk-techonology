<?php
require_once('Grupo.php');

	class GrupoDAO{

		//funcao para inserir um novo cadastro
		public static function insert($grupo){
			global $conexao;
			$insert = 'INSERT INTO grupo(';
			$insert .= 'id, ';
			$insert .= 'nome, ';
			$insert .= 'codigo, ';
			$insert .= 'status';
			$insert .= ') ';
			$insert .= 'VALUES (';
			$insert .= '"' . $grupo->id . '", ';
			$insert .= '"' . $grupo->nome . '", ';
			$insert .= '"' . $grupo->codigo . '", ';
			$insert .= '"' . $grupo->status . '"';
			$insert .= ')';

			//echo $insert;exit;

			$rs_insert = mysql_query($insert, $conexao) or die(mysql_error());

			if($rs_insert){
				$idGrupo = mysql_insert_id($conexao);
				return($idGrupo);
			}else{
				return(-1);
			}
		}

		public static function update($grupo){
			global $conexao;

			$update = 'UPDATE grupo SET ';
			$update .= 'id = "' . $grupo->id . '", ';
			$update .= 'nome = "' . $grupo->nome . '", ';
			$update .= 'codigo = "' . $grupo->codigo . '", ';
			$update .= 'status = "' . $grupo->status . '"';
			$update .= 'WHERE id = ' . $grupo->id;

			//echo $update;exit;

			$rs_update = mysql_query($update, $conexao) or die(mysql_error());

			// se foi atualizado com sucesso retorno o id do cadastro, senao retorno -1 para possiveis verificacoes
			if($rs_update){
				return ($grupo->id);
			}else{
				return(-1);
			}
		}

		public static function delete($id){
			global $conexao;

			$delete = 'UPDATE grupo SET status = 0 WHERE id = '.$id;

			$rs_busca = mysql_query($delete, $conexao) or die(mysql_error());
			if($rs_busca){
				return('Registro excluído com sucesso');
			}else{
				return('Ocorreu um erro ao tentar excluir, tente novamente');
			}
		}

		public static function save($grupo) {
			$idGrupo = -1;
			// verifico se o id foi passado ao chamar a função.
			// se não foi passado quer dizer que é um novo cadastro, senão é a atualização de um cadastro.
			if (empty($grupo->id)) {
				$idGrupo = GrupoDAO::insert($grupo);
			}
			else {
				$idGrupo = GrupoDAO::update($grupo);
			}

			// retorno o id do grupo para possiveis verificacoes.
			return($idGrupo);
		}

		public static function getGrupos($maxGrupos = 0, $pagina = 1, $nome = ''){
			global $conexao;

			$select = 'SELECT SQL_CALC_FOUND_ROWS * FROM grupo';
			$select .= ' WHERE 1 = 1';
			if(!empty($nome)){
				$select .= ' AND nome LIKE "%' . $nome . '%"';
			}
			$select .= ' ORDER BY nome ASC';
			if ($maxGrupos > 0) {
				$regInicial = (($pagina - 1) * $maxGrupos) + 1; //primeiro registro que sera exibido neste resultado

				$select .= ' LIMIT ' . ($regInicial - 1) . ', ' . ($maxGrupos);
			}
			//echo $select; exit;
			$rs_select = mysql_query($select, $conexao) or die(mysql_error());

			$grupos = Array();

			while($grupo = mysql_fetch_object($rs_select, 'Grupo')){
				$grupos[] = $grupo;
			}

			return($grupos);
		}

		public static function getGrupo($id){
			global $conexao;

			$select = 'SELECT * FROM grupo WHERE id = ' . $id;

			$rs_select = mysql_query($select, $conexao) or die(mysql_error());
			if($row = mysql_fetch_object($rs_select, 'Grupo')){
				$grupo = $row;
				return($grupo);
			}else{
				return(0);
			}
		}

		public static function getGrupoByIdUsuario($idUsuario){
			global $conexao;

			$select = 'SELECT grupo.id, grupo.codigo FROM pessoa, pessoaGrupo, grupo';
			$select .= ' WHERE pessoa.id = ' . $idUsuario;
			$select .= ' AND pessoa.id = pessoaGrupo.idPessoa';
			$select .= ' AND grupo.id = pessoaGrupo.idGrupo';

			$rs_select = mysql_query($select, $conexao) or die(mysql_error());
			if($row = mysql_fetch_object($rs_select, 'Grupo')){
				$grupo = $row;
				return($grupo);
			}else{
				return(0);
			}
		}
		
		public static function getGrupoNome($idUsuario){
			global $conexao;

			$select = 'SELECT grupo.nome FROM pessoa, pessoaGrupo, grupo';
			$select .= ' WHERE pessoa.id = ' . $idUsuario;
			$select .= ' AND pessoa.id = pessoaGrupo.idPessoa';
			$select .= ' AND grupo.id = pessoaGrupo.idGrupo';

			$rs_select = mysql_query($select, $conexao) or die(mysql_error());
			if($row = mysql_fetch_object($rs_select, 'Grupo')){
				$grupo = $row;
				return($grupo);
			}else{
				return(0);
			}
		}

		public static function getNumRegEncontrados() {
			global $conexao;

			$pesquisa = 'SELECT FOUND_ROWS() as numRegistros';

			$rs_pesquisa = mysql_query($pesquisa, $conexao) or die(mysql_error());

			if ($row = mysql_fetch_assoc($rs_pesquisa)) {
				return($row['numRegistros']);
			}
			else {
				return(0);
			}
		}

	}
?>
