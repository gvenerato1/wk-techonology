<?php
require_once('Venda.php');

	class VendaDAO{

		//funcao para inserir um novo cadastro
		public static function insert($venda){
			global $conexao;
			$insert = 'INSERT INTO venda(';
			$insert .= 'idCliente, ';
			$insert .= 'dataVenda, ';
			$insert .= 'total, ';
			$insert .= 'status ';
			$insert .= ') ';
			$insert .= 'VALUES (';
			$insert .= '"' . $venda->idCliente . '", ';
			$insert .= '"' . $venda->dataVenda . '", ';
			$insert .= '"' . $venda->total . '", ';
			$insert .= '"' . $venda->status . '" ';
			$insert .= ')';

			$rs_insert = mysql_query($insert, $conexao) or die(mysql_error());

			if($rs_insert){
				$idVenda = mysql_insert_id($conexao);
				if($idVenda){
					VendaDAO::insertProdutos($idVenda, $venda->idProdutos);
					return($idVenda);
				}else{
					return -1;
				}
			}else{
				return(-1);
			}
		}
		
		public static function insertProdutos($idVenda, $idProdutos){
			global $conexao;
			$cont = 0;
			foreach($idProdutos as $idProduto) {
				$insert = 'INSERT INTO vendaProduto(';
				$insert .= 'idVenda, ';
				$insert .= 'idProduto ';
				$insert .= ') ';
				$insert .= 'VALUES (';
				$insert .= '"' . $idVenda . '", ';
				$insert .= '"' . $idProduto . '" ';
				$insert .= ')';

				$rs_insert = mysql_query($insert, $conexao) or die(mysql_error());
				if($rs_insert){
					$cont++;
				}
			}

			if($cont > 0){
				return $cont;
			}else{
				return -1;
			}
		}

		public static function update($venda){
			global $conexao;

			$update = 'UPDATE venda SET ';
			$update .= 'idCliente = "' . $venda->idCliente.'", ';
			$update .= 'total = "' . $venda->total.'", ';
			$update .= 'status = "' . $venda->status . '" ';
			$update .= 'WHERE id = '.$venda->id;

			$rs_update = mysql_query($update, $conexao) or die(mysql_error());

			// se foi atualizado com sucesso retorno o id do cadastro, senao retorno -1 para possiveis verificacoes
			if($rs_update){
				if($venda->id){
					VendaDAO::clearProdutos($venda->id);
					VendaDAO::insertProdutos($venda->id, $venda->idProdutos);
					return($venda->id);
				}else{
					return -1;
				}
			}else{
				return(-1);
			}
		}

		public static function delete($id){
			global $conexao;

			$delete = 'UPDATE venda SET status = 0 WHERE id = '.$id;

			$rs_busca = mysql_query($delete, $conexao) or die(mysql_error());
			if($rs_busca){
				return('Registro excluído com sucesso');
			}else{
				return('Ocorreu um erro ao tentar excluir, tente novamente');
			}
		}

		public static function save($venda) {
			$idVenda = -1;
			// verifico se o id foi passado ao chamar a função.
			// se não foi passado quer dizer que é um novo cadastro, senão é a atualização de um cadastro.
			if (empty($venda->id)){
				$idVenda = VendaDAO::insert($venda);
			}else {
				$idVenda = VendaDAO::update($venda);
			}

			// retorno o id da noticia para possiveis verificacoes.
			return($idVenda);
		}

		public static function getVendas($maxVendas = 0, $pagina = 1){
			global $conexao;

			$select = "SELECT SQL_CALC_FOUND_ROWS venda.* FROM venda";
			$select .= ' WHERE 1 = 1';
			$select .= ' AND venda.status = 1';
			$select .= " ORDER BY venda.id ASC";
			if ($maxVendas > 0) {
				$regInicial = (($pagina - 1) * $maxVendas) + 1; //primeiro registro que sera exibido neste resultado

				$select .= ' LIMIT ' . ($regInicial - 1) . ', ' . ($maxVendas);
			}
			//echo $select; exit;
			$rs_select = mysql_query($select, $conexao) or die(mysql_error());

			$vendas = Array();

			while($venda = mysql_fetch_object($rs_select, 'Venda')){
				$vendas[] = $venda;
			}

			return($vendas);
		}

		public static function getVenda($id){
			global $conexao;

			$select = 'SELECT * FROM venda WHERE id = "' . $id . '"';

			$rs_select = mysql_query($select, $conexao) or die(mysql_error());
			if($row = mysql_fetch_object($rs_select, 'Venda')){
				$venda = $row;
				return($venda);
			}else{
				return(0);
			}
		}
		
		public static function getProdutosVenda($idVenda){
			global $conexao;

			$select = "SELECT SQL_CALC_FOUND_ROWS vendaProduto.* FROM vendaProduto";
			$select .= ' WHERE 1 = 1';
			$select .= ' AND vendaProduto.idVenda = ' . $idVenda;
			$select .= " ORDER BY vendaProduto.idProduto ASC";
			
			$rs_select = mysql_query($select, $conexao) or die(mysql_error());

			$vendas = Array();

			while($venda = mysql_fetch_object($rs_select, 'Venda')){
				$vendas[] = $venda;
			}

			return($vendas);
		}
		
		public static function getNumRegEncontrados() {
			global $conexao;

			$pesquisa = 'SELECT FOUND_ROWS() as numRegistros';

			$rs_pesquisa = mysql_query($pesquisa, $conexao) or die(mysql_error());

			if ($row = mysql_fetch_assoc($rs_pesquisa)) {
				return($row['numRegistros']);
			}
			else {
				return(0);
			}
		}
	}
?>
