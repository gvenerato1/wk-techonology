<?php
	require_once(dirname(__FILE__) . '/../../../classes/config.php');
	require_once(dirname(__FILE__) . '/../../../classes/injecao.php');
	require_once(dirname(__FILE__) . '/../../../classes/Core.php');
	require_once(dirname(__FILE__) . '/../../../classes/Data.php');
	require_once(dirname(__FILE__) . '/../../../classes/Valor.php');
	require_once(dirname(__FILE__) . '/../../../classes/Upload.php');
	require_once(dirname(__FILE__) . '/../../../classes/Texto.php');
	require_once(dirname(__FILE__) . '/../../../classes/Senha.php');	
	require_once(dirname(__FILE__) . '/../classes/VendaDAO.php');
	require_once(dirname(__FILE__) . '/../classes/Venda.php');
	require_once(dirname(__FILE__) . '/../../produtos/classes/ProdutoDAO.php');
?>

<?php
	
	$total = 0;
	$venda = new Venda();
	$venda->id = limpaSQL(strip_tags(trim(Core::getPost('id'))));
	$venda->idCliente = limpaSQL(strip_tags(trim(Core::getPost('idCliente'))));
	$venda->dataVenda = date('Y-m-d H:i:s');
	$venda->status = limpaSQL(strip_tags(trim(Core::getPost('status'))));
	$produtos = $_POST['idProduto'];
	foreach($produtos as $idProduto){
		$produto = ProdutoDAO::getProduto($idProduto);
		$total += $produto->valor;
		$venda->idProdutos[] = $produto->id;
	}
	$venda->total = $total;
	
	if(!empty($venda->idCliente)){
		$id = $venda->save();
		if($id > 0){
			echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/vendas.php?msg=Operação realizada com sucesso!'>";
		}
		elseif($id == -1){
			echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/vendas.php?msg=Ocorreu um erro, por favor tente novamente!'>";
		}
	}else{
		echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/vendas.php?msg=Preencha os campos obrigatórios!'>";
	}
?>
