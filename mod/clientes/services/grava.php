<?php
	require_once(dirname(__FILE__) . '/../../../classes/config.php');
	require_once(dirname(__FILE__) . '/../../../classes/injecao.php');
	require_once(dirname(__FILE__) . '/../../../classes/Core.php');
	require_once(dirname(__FILE__) . '/../../../classes/Data.php');
	require_once(dirname(__FILE__) . '/../../../classes/Upload.php');
	require_once(dirname(__FILE__) . '/../../../classes/Texto.php');
	require_once(dirname(__FILE__) . '/../../../classes/Senha.php');	
	require_once(dirname(__FILE__) . '/../classes/ClienteDAO.php');
	require_once(dirname(__FILE__) . '/../classes/Cliente.php');
?>

<?php
	
	$cliente = new Cliente();
	$cliente->id = limpaSQL(strip_tags(trim(Core::getPost('id'))));
	$cliente->nome = limpaSQL(strip_tags(trim(Core::getPost('nome'))));
	$cliente->sobrenome = limpaSQL(strip_tags(trim(Core::getPost('sobrenome'))));
	$cliente->cpf = Core::getPost('cpf');
	$cliente->endereco = limpaSQL(strip_tags(trim(Core::getPost('endereco'))));
	$cliente->email = Core::getPost('email');
	$cliente->nascimento = Data::brToSql(Core::getPost('nascimento'));
	$cliente->status = limpaSQL(strip_tags(trim(Core::getPost('status'))));

	if(!empty($cliente->nome) && !empty($cliente->cpf)){
		$id = $cliente->save();
		if($id > 0){
			echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/clientes.php?msg=Operação realizada com sucesso!'>";
		}
		elseif($id == -1){
			echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/clientes.php?msg=Ocorreu um erro, por favor tente novamente!'>";
		}
	}else{
		echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/clientes.php?msg=Preencha os campos obrigatórios!'>";
	}
?>
