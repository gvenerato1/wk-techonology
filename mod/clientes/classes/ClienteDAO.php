<?php
require_once('Cliente.php');

	class ClienteDAO{

		//funcao para inserir um novo cadastro
		public static function insert($cliente){
			global $conexao;
			$insert = 'INSERT INTO cliente(';
			$insert .= 'nome, ';
			$insert .= 'sobrenome, ';
			$insert .= 'cpf, ';
			$insert .= 'endereco, ';
			$insert .= 'email, ';
			$insert .= 'nascimento, ';
			$insert .= 'status ';
			$insert .= ') ';
			$insert .= 'VALUES (';
			$insert .= '"' . $cliente->nome . '", ';
			$insert .= '"' . $cliente->sobrenome . '", ';
			$insert .= '"' . $cliente->cpf . '", ';
			$insert .= '"' . $cliente->endereco . '", ';
			$insert .= '"' . $cliente->email . '", ';
			$insert .= '"' . $cliente->nascimento.'", ';
			$insert .= '"' . $cliente->status . '" ';
			$insert .= ')';

			$rs_insert = mysql_query($insert, $conexao) or die(mysql_error());

			if($rs_insert){
				$idCliente = mysql_insert_id($conexao);
				if($idCliente){
					return($idCliente);
				}else{
					return -1;
				}
			}else{
				return(-1);
			}
		}

		public static function update($cliente){
			global $conexao;

			$update = 'UPDATE cliente SET ';
			$update .= 'nome = "' . $cliente->nome.'", ';
			$update .= 'sobrenome = "' . $cliente->sobrenome . '", ';
			$update .= 'cpf = "' . $cliente->cpf.'", ';
			$update .= 'endereco = "' . $cliente->endereco.'", ';
			$update .= 'email = "' . $cliente->email . '", ';
			$update .= 'nascimento = "' . $cliente->nascimento . '", ';
			$update .= 'status = "' . $cliente->status . '" ';
			$update .= 'WHERE id = '.$cliente->id;

			$rs_update = mysql_query($update, $conexao) or die(mysql_error());

			// se foi atualizado com sucesso retorno o id do cadastro, senao retorno -1 para possiveis verificacoes
			if($rs_update){
				if($cliente->id){
					return($cliente->id);
				}else{
					return -1;
				}
			}else{
				return(-1);
			}
		}

		public static function delete($id){
			global $conexao;

			$delete = 'UPDATE cliente SET status = 0 WHERE id = '.$id;

			$rs_busca = mysql_query($delete, $conexao) or die(mysql_error());
			if($rs_busca){
				return('Registro excluído com sucesso');
			}else{
				return('Ocorreu um erro ao tentar excluir, tente novamente');
			}
		}

		public static function save($cliente) {
			$idCliente = -1;
			// verifico se o id foi passado ao chamar a função.
			// se não foi passado quer dizer que é um novo cadastro, senão é a atualização de um cadastro.
			if (empty($cliente->id)){
				$idCliente = ClienteDAO::insert($cliente);
			}else {
				$idCliente = ClienteDAO::update($cliente);
			}

			// retorno o id da noticia para possiveis verificacoes.
			return($idCliente);
		}

		public static function getClientes($maxClientes = 0, $pagina = 1, $nome = ''){
			global $conexao;

			$select = "SELECT SQL_CALC_FOUND_ROWS cliente.* FROM cliente";
			$select .= ' WHERE 1 = 1';
			$select .= ' AND cliente.status = 1';
			if($nome){
				$select .= " AND (cliente.nome like '%$nome%' OR cliente.sobrenome like '%$nome%')";
			}
			//$select .= " GROUP BY cliente.id";
			$select .= " ORDER BY cliente.nome ASC";
			if ($maxClientes > 0) {
				$regInicial = (($pagina - 1) * $maxClientes) + 1; //primeiro registro que sera exibido neste resultado

				$select .= ' LIMIT ' . ($regInicial - 1) . ', ' . ($maxClientes);
			}
			//echo $select; exit;
			$rs_select = mysql_query($select, $conexao) or die(mysql_error());

			$clientes = Array();

			while($cliente = mysql_fetch_object($rs_select, 'Cliente')){
				$clientes[] = $cliente;
			}

			return($clientes);
		}

		public static function getCliente($id){
			global $conexao;

			$select = 'SELECT * FROM cliente WHERE id = "' . $id . '"';

			$rs_select = mysql_query($select, $conexao) or die(mysql_error());
			if($row = mysql_fetch_object($rs_select, 'Cliente')){
				$cliente = $row;
				return($cliente);
			}else{
				return(0);
			}
		}
		
		public static function getNumRegEncontrados() {
			global $conexao;

			$pesquisa = 'SELECT FOUND_ROWS() as numRegistros';

			$rs_pesquisa = mysql_query($pesquisa, $conexao) or die(mysql_error());

			if ($row = mysql_fetch_assoc($rs_pesquisa)) {
				return($row['numRegistros']);
			}
			else {
				return(0);
			}
		}
	}
?>
