<?php
	class Cliente{
		//declaracao das variaveis
		public $id = 0;
		public $nome = '';
		public $sobrenome = '';
		public $cpf = '';
		public $endereco = '';
		public $email = '';
		public $nascimento = '';
		public $status = 1;

		function save(){
			require_once('ClienteDAO.php');
			return(ClienteDAO::save($this));
		}

	}
?>
