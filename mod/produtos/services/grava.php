<?php
	require_once(dirname(__FILE__) . '/../../../classes/config.php');
	require_once(dirname(__FILE__) . '/../../../classes/injecao.php');
	require_once(dirname(__FILE__) . '/../../../classes/Core.php');
	require_once(dirname(__FILE__) . '/../../../classes/Data.php');
	require_once(dirname(__FILE__) . '/../../../classes/Valor.php');
	require_once(dirname(__FILE__) . '/../../../classes/Upload.php');
	require_once(dirname(__FILE__) . '/../../../classes/Texto.php');
	require_once(dirname(__FILE__) . '/../../../classes/Senha.php');	
	require_once(dirname(__FILE__) . '/../classes/ProdutoDAO.php');
	require_once(dirname(__FILE__) . '/../classes/Produto.php');
?>

<?php
	
	$produto = new Produto();
	$produto->id = limpaSQL(strip_tags(trim(Core::getPost('id'))));
	$produto->sku = limpaSQL(strip_tags(trim(Core::getPost('sku'))));
	$produto->nome = limpaSQL(strip_tags(trim(Core::getPost('nome'))));
	$produto->valor = Valor::brToSql(Core::getPost('valor'));
	$produto->status = limpaSQL(strip_tags(trim(Core::getPost('status'))));

	if(!empty($produto->nome) && !empty($produto->sku) && !empty($produto->sku)){
		$id = $produto->save();
		if($id > 0){
			echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/produtos.php?msg=Operação realizada com sucesso!'>";
		}
		elseif($id == -1){
			echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/produtos.php?msg=Ocorreu um erro, por favor tente novamente!'>";
		}
	}else{
		echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/produtos.php?msg=Preencha os campos obrigatórios!'>";
	}
?>
