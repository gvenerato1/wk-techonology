<?php
require_once('Produto.php');

	class ProdutoDAO{

		//funcao para inserir um novo cadastro
		public static function insert($produto){
			global $conexao;
			$insert = 'INSERT INTO produto(';
			$insert .= 'sku, ';
			$insert .= 'nome, ';
			$insert .= 'valor, ';
			$insert .= 'status ';
			$insert .= ') ';
			$insert .= 'VALUES (';
			$insert .= '"' . $produto->sku . '", ';
			$insert .= '"' . $produto->nome . '", ';
			$insert .= '"' . $produto->valor . '", ';
			$insert .= '"' . $produto->status . '" ';
			$insert .= ')';

			$rs_insert = mysql_query($insert, $conexao) or die(mysql_error());

			if($rs_insert){
				$idProduto = mysql_insert_id($conexao);
				if($idProduto){
					return($idProduto);
				}else{
					return -1;
				}
			}else{
				return(-1);
			}
		}

		public static function update($produto){
			global $conexao;

			$update = 'UPDATE produto SET ';
			$update .= 'sku = "' . $produto->sku.'", ';
			$update .= 'nome = "' . $produto->nome . '", ';
			$update .= 'valor = "' . $produto->valor.'", ';
			$update .= 'status = "' . $produto->status . '" ';
			$update .= 'WHERE id = '.$produto->id;

			//echo $update;exit;

			$rs_update = mysql_query($update, $conexao) or die(mysql_error());

			// se foi atualizado com sucesso retorno o id do cadastro, senao retorno -1 para possiveis verificacoes
			if($rs_update){
				if($produto->id){
					return($produto->id);
				}else{
					return -1;
				}
			}else{
				return(-1);
			}
		}

		public static function delete($id){
			global $conexao;

			$delete = 'UPDATE produto SET status = 0 WHERE id = '.$id;

			$rs_busca = mysql_query($delete, $conexao) or die(mysql_error());
			if($rs_busca){
				return('Registro excluído com sucesso');
			}else{
				return('Ocorreu um erro ao tentar excluir, tente novamente');
			}
		}

		public static function save($produto) {
			$idProduto = -1;
			// verifico se o id foi passado ao chamar a função.
			// se não foi passado quer dizer que é um novo cadastro, senão é a atualização de um cadastro.
			if (empty($produto->id)){
				$idProduto = ProdutoDAO::insert($produto);
			}else {
				$idProduto = ProdutoDAO::update($produto);
			}

			// retorno o id da noticia para possiveis verificacoes.
			return($idProduto);
		}

		public static function getProdutos($maxProdutos = 0, $pagina = 1, $nome = ''){
			global $conexao;

			$select = "SELECT SQL_CALC_FOUND_ROWS produto.* FROM produto";
			$select .= ' WHERE 1 = 1';
			$select .= ' AND produto.status = 1';
			if($nome){
				$select .= " AND (produto.nome like '%$nome%' OR produto.sku like '%$nome%')";
			}
			$select .= " ORDER BY produto.nome ASC";
			if ($maxProdutos > 0) {
				$regInicial = (($pagina - 1) * $maxProdutos) + 1; //primeiro registro que sera exibido neste resultado

				$select .= ' LIMIT ' . ($regInicial - 1) . ', ' . ($maxProdutos);
			}
			//echo $select; exit;
			$rs_select = mysql_query($select, $conexao) or die(mysql_error());

			$produtos = Array();

			while($produto = mysql_fetch_object($rs_select, 'Produto')){
				$produtos[] = $produto;
			}

			return($produtos);
		}

		public static function getProduto($id){
			global $conexao;

			$select = 'SELECT * FROM produto WHERE id = "' . $id . '"';

			$rs_select = mysql_query($select, $conexao) or die(mysql_error());
			if($row = mysql_fetch_object($rs_select, 'Produto')){
				$produto = $row;
				return($produto);
			}else{
				return(0);
			}
		}
		
		public static function getNumRegEncontrados() {
			global $conexao;

			$pesquisa = 'SELECT FOUND_ROWS() as numRegistros';

			$rs_pesquisa = mysql_query($pesquisa, $conexao) or die(mysql_error());

			if ($row = mysql_fetch_assoc($rs_pesquisa)) {
				return($row['numRegistros']);
			}
			else {
				return(0);
			}
		}
	}
?>
