<?php
	require_once(dirname(__FILE__) . '/../../../classes/config.php');
	require_once(dirname(__FILE__) . '/../../../classes/injecao.php');
	require_once(dirname(__FILE__) . '/../../../classes/Core.php');
	require_once(dirname(__FILE__) . '/../../../classes/Data.php');
	require_once(dirname(__FILE__) . '/../../../classes/Upload.php');
	require_once(dirname(__FILE__) . '/../../../classes/Texto.php');
	require_once(dirname(__FILE__) . '/../../../classes/Senha.php');	
	require_once(dirname(__FILE__) . '/../classes/PessoaDAO.php');
	require_once(dirname(__FILE__) . '/../classes/Pessoa.php');
?>

<?php
	
	$pessoa = new Pessoa();
	$pessoa->id = limpaSQL(strip_tags(trim(Core::getPost('id'))));
	$pessoa->nome = limpaSQL(strip_tags(trim(Core::getPost('nome'))));
	$pessoa->sobrenome = limpaSQL(strip_tags(trim(Core::getPost('sobrenome'))));
	$pessoa->email = Core::getPost('email');
	$pessoa->login = Core::getPost('login');
	$pessoa->telefone = Core::getPost('telefone');
	$pessoa->status = limpaSQL(strip_tags(trim(Core::getPost('status'))));
	
	$pessoa->grupo = limpaSQL(strip_tags(trim(Core::getPost('grupo'))));
	$pessoa->unidade = limpaSQL(strip_tags(trim(Core::getPost('unidade'))));

	$senha = Core::getPost('senha');
	if($senha == '')
		$pessoa->senha = $senha;
	else
		$pessoa->senha = Senha::criptogradaSenha(limpaSQL($senha));

	if(!empty($pessoa->nome) && !empty($pessoa->grupo)){
		if(!PessoaDAO::getPessoaByLogin($pessoa->login) || (PessoaDAO::getPessoaByLogin($pessoa->login) && $pessoa->id!=0) || (empty($pessoa->login))){
			$pessoa->save();
			if($pessoa->login){
				echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/usuarios.php?msg=Operação realizada com sucesso!'>";
			}else{
				echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/usuarios-representantes.php?msg=Operação realizada com sucesso!'>";
			}
		}else{
			if(PessoaDAO::getPessoaByLogin($pessoa->login) && $pessoa->id == 0)
				echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/usuarios.php?msg=ERRO(LOGIN EXISTENTE)'>";
			else
				echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/usuarios.php?msg=Ocorreu um erro, por favor tente novamente'>";
		}
	}else{
		echo "<meta http-equiv='refresh' content='0;URL=../../../sistema/usuarios.php?msg=Preencha os campos obrigatórios!'>";
	}
?>
