<?php
	class Pessoa{
		//declaracao das variaveis
		public $id = 0;
		public $login = '';
		public $senha = '';
		public $nome = '';
		public $sobrenome = '';
		public $email = '';
		public $telefone = '';
		public $status = 1;
		
		public $grupo = '';
		public $unidade = '';

		function save(){
			require_once('PessoaDAO.php');
			return(PessoaDAO::save($this));
		}

	}
?>
