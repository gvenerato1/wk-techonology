<?php
require_once('Pessoa.php');

	class PessoaDAO{

		//funcao para inserir um novo cadastro
		public static function insert($pessoa){
			global $conexao;
			$insert = 'INSERT INTO pessoa(';
			$insert .= 'login, ';
			$insert .= 'senha, ';
			$insert .= 'nome, ';
			$insert .= 'sobrenome, ';
			$insert .= 'email, ';
			$insert .= 'telefone, ';
			$insert .= 'status ';
			$insert .= ') ';
			$insert .= 'VALUES (';
			$insert .= '"'.$pessoa->login.'", ';
			$insert .= '"'.$pessoa->senha.'", ';
			$insert .= '"'.$pessoa->nome.'", ';
			$insert .= '"'.$pessoa->sobrenome.'", ';
			$insert .= '"'.$pessoa->email.'", ';
			$insert .= '"'.$pessoa->telefone.'", ';
			$insert .= '"'.$pessoa->status.'" ';
			$insert .= ')';

			$rs_insert = mysql_query($insert, $conexao) or die(mysql_error());

			if($rs_insert){
				//salva o grupo
				$idPessoa = mysql_insert_id($conexao);
				if($pessoa->grupo){
					$grupoId = PessoaDAO::insertGroup($idPessoa, $pessoa->grupo);
				}
				if($pessoa->unidade){
					$unidadeId = PessoaDAO::insertUnit($idPessoa, $pessoa->unidade);
				}
				if($idPessoa){
					return($idPessoa);
				}else{
					return -1;
				}
			}else{
				return(-1);
			}
		}
		
		public static function insertGroup($idPessoa, $idGrupo){
			global $conexao;
			PessoaDAO::removeVinculoGrupo($idPessoa);
			
			$insert = 'INSERT INTO pessoaGrupo(';
			$insert .= 'idPessoa, ';
			$insert .= 'idGrupo ';
			$insert .= ') ';
			$insert .= 'VALUES (';
			$insert .= '"'.$idPessoa.'", ';
			$insert .= '"'.$idGrupo.'" ';
			$insert .= ')';

			$rs_insert = mysql_query($insert, $conexao) or die(mysql_error());

			if($rs_insert){
				//salva o grupo
				$idGrupo = mysql_insert_id($conexao);
				if($idGrupo){
					return($idGrupo);
				}else{
					return -1;
				}
			}else{
				return(-1);
			}
		}
		
		public static function insertUnit($idPessoa, $idUnidade){
			global $conexao;
			PessoaDAO::removeVinculoUnidade($idPessoa);
			
			$insert = 'INSERT INTO pessoaUnidade(';
			$insert .= 'idPessoa, ';
			$insert .= 'idUnidade ';
			$insert .= ') ';
			$insert .= 'VALUES (';
			$insert .= '"'.$idPessoa.'", ';
			$insert .= '"'.$idUnidade.'" ';
			$insert .= ')';
			
			$rs_insert = mysql_query($insert, $conexao) or die(mysql_error());

			if($rs_insert){
				//salva a unidade
				$idUnidade = mysql_insert_id($conexao);
				if($idUnidade){
					return($idUnidade);
				}else{
					return -1;
				}
			}else{
				return(-1);
			}
		}

		public static function update($pessoa){
			global $conexao;

			$update = 'UPDATE pessoa SET ';
			$update .= 'login = "'.$pessoa->login.'", ';
			if($pessoa->senha != '')
				$update .= 'senha = "'.$pessoa->senha.'", ';
			$update .= 'nome = "'.$pessoa->nome.'", ';
			$update .= 'sobrenome = "'.$pessoa->sobrenome.'", ';
			$update .= 'email = "'.$pessoa->email.'", ';
			$update .= 'telefone = "'.$pessoa->telefone.'", ';
			$update .= 'status = "'.$pessoa->status.'" ';
			$update .= 'WHERE id = '.$pessoa->id;

			//echo $update;exit;

			$rs_update = mysql_query($update, $conexao) or die(mysql_error());

			// se foi atualizado com sucesso retorno o id do cadastro, senao retorno -1 para possiveis verificacoes
			if($rs_update){
				if($pessoa->grupo){
					$grupoId = PessoaDAO::insertGroup($pessoa->id, $pessoa->grupo);
				}
				if($pessoa->unidade){
					$unidadeId = PessoaDAO::insertUnit($pessoa->id, $pessoa->unidade);
				}
				if($pessoa->id){
					return($pessoa->id);
				}else{
					return -1;
				}
			}else{
				return(-1);
			}
		}

		public static function delete($id){
			global $conexao;

			$delete = 'UPDATE pessoa SET status = 0 WHERE id = '.$id;

			$rs_busca = mysql_query($delete, $conexao) or die(mysql_error());
			if($rs_busca){
				return('Registro excluído com sucesso');
			}else{
				return('Ocorreu um erro ao tentar excluir, tente novamente');
			}
		}

		public static function removeVinculoGrupo($idPessoa){
			global $conexao;

			$delete = 'DELETE FROM pessoaGrupo WHERE idPessoa = ' . $idPessoa;

			$rs_busca = mysql_query($delete, $conexao) or die(mysql_error());
			if($rs_busca){
				return true;
			}else{
				return false;
			}
		}
		
		public static function removeVinculoUnidade($idPessoa){
			global $conexao;

			$delete = 'DELETE FROM pessoaUnidade WHERE idPessoa = ' . $idPessoa;

			$rs_busca = mysql_query($delete, $conexao) or die(mysql_error());
			if($rs_busca){
				return true;
			}else{
				return false;
			}
		}

		public static function save($pessoa) {
			$idPessoa = -1;
			// verifico se o id foi passado ao chamar a função.
			// se não foi passado quer dizer que é um novo cadastro, senão é a atualização de um cadastro.
			if (empty($pessoa->id)){
				if(PessoaDAO::getPessoaByLogin($pessoa->login) == 0 || empty($pessoa->login)){
					$idPessoa = PessoaDAO::insert($pessoa);
				}else {
					echo "<script>alert('Login ja cadastrado');</script>";
				}
			}else {
				$idPessoa = PessoaDAO::update($pessoa);
			}

			// retorno o id da noticia para possiveis verificacoes.
			return($idPessoa);
		}

		public static function getPessoas($maxPessoas = 0, $pagina = 1, $nome = '', $grupoNotIn = ''){
			global $conexao;

			$select = "SELECT SQL_CALC_FOUND_ROWS pessoa.* FROM pessoa, grupo, pessoaGrupo";
			$select .= ' WHERE 1 = 1';
			$select .= ' AND pessoa.status = 1';
			if($nome){
				$select .= " AND (pessoa.nome like '%$nome%' OR pessoa.sobrenome like '%$nome%' OR pessoa.login like '%$nome%')";
			}
			if(is_array($grupoNotIn)){
				$comma_separated = implode(",", $grupoNotIn);
				$select .= " AND grupo.id NOT IN (" . $comma_separated . ")";
				$select .= " AND pessoa.id = pessoaGrupo.idPessoa";
				$select .= " AND grupo.id = pessoaGrupo.idGrupo";
			}
			$select .= " GROUP BY pessoa.id";
			$select .= " ORDER BY pessoa.nome ASC";
			if ($maxPessoas > 0) {
				$regInicial = (($pagina - 1) * $maxPessoas) + 1; //primeiro registro que sera exibido neste resultado

				$select .= ' LIMIT ' . ($regInicial - 1) . ', ' . ($maxPessoas);
			}
			//echo $select; exit;
			$rs_select = mysql_query($select, $conexao) or die(mysql_error());

			$pessoas = Array();

			while($pessoa = mysql_fetch_object($rs_select, 'Pessoa')){
				$pessoas[] = $pessoa;
			}

			return($pessoas);
		}

		public static function getPessoa($id){
			global $conexao;

			$select = 'SELECT * FROM pessoa WHERE id = "' . $id . '"';

			$rs_select = mysql_query($select, $conexao) or die(mysql_error());
			if($row = mysql_fetch_object($rs_select, 'Pessoa')){
				$pessoa = $row;
				return($pessoa);
			}else{
				return(0);
			}
		}

		public static function getPessoaByLogin($login){
			global $conexao;

			$select = 'SELECT * FROM pessoa WHERE login = "' . $login . '"';

			$rs_select = mysql_query($select, $conexao) or die(mysql_error());
			if($row = mysql_fetch_object($rs_select, 'Pessoa')){
				$pessoa = $row;
				return($pessoa);
			}else{
				return(0);
			}
		}

		public static function getRepresentantesByUnidadeGrupo($idUnidade, $idGrupo){
			global $conexao;

			$select = "SELECT pessoa.id, pessoa.nome, pessoa.sobrenome FROM pessoa, pessoaGrupo, pessoaUnidade, grupo, unidade";
			$select .= ' WHERE (grupo.id = ' . $idGrupo . ' AND pessoa.id = pessoaGrupo.idPessoa AND grupo.id = pessoaGrupo.idGrupo)';
			$select .= ' AND (unidade.id = ' . $idUnidade . ' AND pessoa.id = pessoaUnidade.idPessoa AND unidade.id = pessoaUnidade.idUnidade)';
			$select .= ' GROUP BY pessoa.id';
			$select .= " ORDER BY pessoa.nome ASC";
			
			//echo $select; exit;
			$rs_select = mysql_query($select, $conexao) or die(mysql_error());

			$pessoas = Array();

			while($pessoa = mysql_fetch_object($rs_select, 'Pessoa')){
				$pessoas[] = $pessoa;
			}

			return($pessoas);
		}
		
		public static function getPecasByPessoa($maxPecas = 0, $pagina = 1, $nome = '', $status = '', $idUnidade = '', $dataInicial = '', $dataFinal = ''){
			global $conexao;

			$select = 'SELECT SQL_CALC_FOUND_ROWS pessoa.* FROM pessoa, peca';
			$select .= ' WHERE pessoa.id = peca.idPessoa';
			if(is_int($status) && $status >= 0){
				$select .= ' AND peca.status = ' . $status;
			}
			if(!empty($nome)){
				$select .= ' AND pessoa.nome LIKE "%' . $nome . '%"';
			}
			if(!empty($idUnidade) && $idUnidade > 0){
				$select.= ' AND peca.idUnidade = ' . $idUnidade;
			}
			if(!empty($dataInicial) && !empty($dataFinal)){
				$select .= ' AND peca.data between "' . $dataInicial . ' 00:00:01" AND "' . $dataFinal . ' 23:59:59"';
			}elseif(!empty($dataInicial) && empty($dataFinal)){
				$select .= ' AND peca.data = "' . $dataInicial . '"';
			}elseif(!empty($dataFinal) && empty($dataInicial)){
				$select .= ' AND peca.data = "' . $dataFinal . '"';
			}
			$select .= ' GROUP BY pessoa.id';
			$select .= ' ORDER BY peca.data DESC';
			if ($maxPecas > 0) {
				$regInicial = (($pagina - 1) * $maxPecas) + 1; //primeiro registro que sera exibido neste resultado

				$select .= ' LIMIT ' . ($regInicial - 1) . ', ' . ($maxPecas);
			}
			//echo $select; exit;
			$rs_select = mysql_query($select, $conexao) or die(mysql_error());

			$pecas = Array();

			while($peca = mysql_fetch_object($rs_select, 'Peca')){
				$pecas[] = $peca;
			}

			return($pecas);
		}
		
		public static function getPecasByPessoaGerente($maxPecas = 0, $pagina = 1, $nome = '', $idGerente = '', $status = '', $dataInicial = '', $dataFinal = ''){
			global $conexao;
			
			$select = 'SELECT SQL_CALC_FOUND_ROWS pessoa.* FROM pessoa, peca';
			$select .= ' WHERE pessoa.id = peca.idPessoa';
			$select .= ' AND peca.idUnidade = (';
			$select .= 'SELECT unidade.id FROM pessoa, unidade, pessoaUnidade 
				WHERE pessoa.id = ' . $idGerente . ' 
				AND pessoa.id = pessoaUnidade.idPessoa 
				AND unidade.id = pessoaUnidade.idUnidade 
				GROUP BY pessoa.id 
				ORDER BY pessoa.nome DESC';
			$select .= ')';
			if($status >= 0){
				$select .= ' AND peca.status = ' . $status;
			}
			if(!empty($nome)){
				$select .= ' AND pessoa.nome LIKE "%' . $nome . '%"';
			}
			/* LEIA - Somente para os usuários do tipo gerente são retornado os valores baseado na coluna data, o restante utiliza a coluna dataUpdate */
			if(!empty($dataInicial) && !empty($dataFinal)){
				$select .= ' AND peca.data between "' . $dataInicial . ' 00:00:01" AND "' . $dataFinal . ' 23:59:59"';
			}elseif(!empty($dataInicial) && empty($dataFinal)){
				$select .= ' AND DATE(peca.data) = "' . $dataInicial . '"';
			}elseif(!empty($dataFinal) && empty($dataInicial)){
				$select .= ' AND DATE(peca.data) = " ' . $dataFinal . '"';
			}
			$select .= ' GROUP BY pessoa.id';
			$select .= ' ORDER BY peca.data DESC';
			if ($maxPecas > 0) {
				$regInicial = (($pagina - 1) * $maxPecas) + 1; //primeiro registro que sera exibido neste resultado

				$select .= ' LIMIT ' . ($regInicial - 1) . ', ' . ($maxPecas);
			}
			//echo $select; exit;
			$rs_select = mysql_query($select, $conexao) or die(mysql_error());

			$pecas = Array();

			while($peca = mysql_fetch_object($rs_select, 'Peca')){
				$pecas[] = $peca;
			}

			return($pecas);
		}
		
		public static function getPecasByPessoaCobranca($maxPecas = 0, $pagina = 1, $nome = '', $status = '', $idUnidade = '', $dataInicial = '', $dataFinal = '', $codigoPeca = '', $motivo = ''){
			global $conexao;
			
			$select = 'SELECT SQL_CALC_FOUND_ROWS pessoa.*, SUM(valor) as total, peca.id as pecas FROM pessoa, peca, pecasId';
			$select .= ' WHERE pessoa.id = peca.idPessoa';
			if(is_int($status) && $status >= 0){
				$select .= ' AND peca.status = ' . $status;
			}
			if(!empty($nome)){
				$select .= ' AND pessoa.nome LIKE "%' . $nome . '%"';
			}
			if(!empty($idUnidade) && $idUnidade > 0){
				$select.= ' AND peca.idUnidade = ' . $idUnidade;
			}
			if(!empty($dataInicial) && !empty($dataFinal)){
				$select .= ' AND peca.dataUpdate between "' . $dataInicial . ' 00:00:01" AND "' . $dataFinal . ' 23:59:59"';
				/*$select .= ' AND (peca.data between "' . $dataInicial . ' 00:00:01" AND "' . $dataFinal . ' 23:59:59"';
				$select .= ' OR peca.dataUpdate between "' . $dataInicial . ' 00:00:01" AND "' . $dataFinal . ' 23:59:59")';*/
			}elseif(!empty($dataInicial) && empty($dataFinal)){
				$select .= ' AND DATE(peca.dataUpdate) = "' . $dataInicial . '"';
				//$select .= ' AND (DATE(peca.data) = "' . $dataInicial . '" OR DATE(peca.dataUpdate) = "' . $dataInicial . '")';
			}elseif(!empty($dataFinal) && empty($dataInicial)){
				$select .= ' AND DATE(peca.dataUpdate) = " ' . $dataFinal . '"';
				//$select .= ' AND (DATE(peca.data) = "' . $dataFinal . '" OR DATE(peca.dataUpdate) = " ' . $dataFinal . '")';
			}
			if(!empty($codigoPeca)){
				$select .= ' AND pecasId.codigoPeca = ' . $codigoPeca;
				$select .= ' AND pecasId.idPeca = peca.id';
			}
			if(!empty($motivo)){
				$select .= ' AND peca.motivo = ' . $motivo;
			}
			$select .= ' GROUP BY pessoa.id';
			$select .= ' ORDER BY peca.dataUpdate DESC';
			if ($maxPecas > 0) {
				$regInicial = (($pagina - 1) * $maxPecas) + 1; //primeiro registro que sera exibido neste resultado

				$select .= ' LIMIT ' . ($regInicial - 1) . ', ' . ($maxPecas);
			}
			//echo $select; exit;
			$rs_select = mysql_query($select, $conexao) or die(mysql_error());

			$pecas = Array();

			while($peca = mysql_fetch_object($rs_select, 'Peca')){
				$pecas[] = $peca;
			}

			return($pecas);
		}
		
		public static function getPecasByPessoaCobrancaParcelas($maxPecas = 0, $pagina = 1, $nome = '', $status = '', $idUnidade = '', $dataInicial = '', $dataFinal = ''){
			global $conexao;
			
			$select = 'SELECT SQL_CALC_FOUND_ROWS pessoa.* FROM pessoa, peca, pecaQuitacao';
			$select .= ' WHERE pessoa.id = peca.idPessoa';
			$select .= ' AND peca.id = pecaQuitacao.idPeca';
			if(is_int($status) && $status >= 0){
				$select .= ' AND pecaQuitacao.status = ' . $status;
			}
			if(!empty($nome)){
				$select .= ' AND pessoa.nome LIKE "%' . $nome . '%"';
			}
			if(!empty($idUnidade) && $idUnidade > 0){
				$select.= ' AND peca.idUnidade = ' . $idUnidade;
			}
			if(!empty($dataInicial) && !empty($dataFinal)){
				$select .= ' AND peca.data between "' . $dataInicial . ' 00:00:01" AND "' . $dataFinal . ' 23:59:59"';
			}elseif(!empty($dataInicial) && empty($dataFinal)){
				$select .= ' AND peca.data = "' . $dataInicial . '"';
			}elseif(!empty($dataFinal) && empty($dataInicial)){
				$select .= ' AND peca.data = "' . $dataFinal . '"';
			}
			$select .= ' GROUP BY pessoa.id';
			$select .= ' ORDER BY peca.data DESC';
			if ($maxPecas > 0) {
				$regInicial = (($pagina - 1) * $maxPecas) + 1; //primeiro registro que sera exibido neste resultado

				$select .= ' LIMIT ' . ($regInicial - 1) . ', ' . ($maxPecas);
			}
			//echo $select; exit;
			$rs_select = mysql_query($select, $conexao) or die(mysql_error());

			$pecas = Array();

			while($peca = mysql_fetch_object($rs_select, 'Peca')){
				$pecas[] = $peca;
			}

			return($pecas);
		}
		
		public static function getTotalPecasPendentes($pessoaId, $status = '', $unidade = '', $dataInicial = '', $dataFinal = '', $codigoPeca = '', $motivo = ''){
			global $conexao;			
			$select = 'SELECT COUNT(peca.id) AS total, SUM(peca.valor) as valor, peca.id, peca.pecas FROM peca';
			$select .= ' WHERE peca.idPessoa = ' . $pessoaId;
			if(is_int($status) && $status >= 0){
				$select .= ' AND peca.status = ' . $status;
			}else{
				$select .= ' AND peca.status != 999';
			}
			if(!empty($unidade) && $unidade > 0){
				$select.= ' AND peca.idUnidade = ' . $unidade;
			}
			if(!empty($dataInicial) && !empty($dataFinal)){
				$select .= ' AND peca.dataUpdate between "' . $dataInicial . ' 00:00:01" AND "' . $dataFinal . ' 23:59:59"';
				/*$select .= ' AND (peca.data between "' . $dataInicial . ' 00:00:01" AND "' . $dataFinal . ' 23:59:59"';
				$select .= ' OR peca.dataUpdate between "' . $dataInicial . ' 00:00:01" AND "' . $dataFinal . ' 23:59:59")';*/
			}elseif(!empty($dataInicial) && empty($dataFinal)){
				$select .= ' AND DATE(peca.dataUpdate) = "' . $dataInicial . '"';
				//$select .= ' AND (DATE(peca.data) = "' . $dataInicial . '" OR DATE(peca.dataUpdate) = "' . $dataInicial . '")';
			}elseif(!empty($dataFinal) && empty($dataInicial)){
				$select .= ' AND DATE(peca.dataUpdate) = " ' . $dataFinal . '"';
				//$select .= ' AND (DATE(peca.data) = "' . $dataFinal . '" OR DATE(peca.dataUpdate) = " ' . $dataFinal . '")';
			}
			if(!empty($codigoPeca)){
				$select .= ' AND ' . $codigoPeca . ' = (SELECT pecasId.codigoPeca FROM pecasId WHERE peca.id = pecasId.idPeca AND pecasId.codigoPeca = ' . $codigoPeca . ')';
				//$select .= ' AND pecasId.codigoPeca = ' . $codigoPeca;
				//$select .= ' AND pecasId.idPeca = peca.id';
			}
			if(!empty($motivo)){
				$select .= ' AND peca.motivo = ' . $motivo;
			}
			$select .= ' AND peca.pecas != ""';
			//echo $select . '<br><br>';exit;

			$rs_select = mysql_query($select, $conexao) or die(mysql_error());
			if($row = mysql_fetch_object($rs_select, 'Peca')){
				$peca = $row;
				require_once('/../../pecas/classes/PecaDAO.php');
				$totalPecas = PecaDAO::getTotalPecasPendentes($pessoaId, $status, $codigoPeca, $motivo);
				$totalValor = PecaDAO::getValorPecasPendentes($pessoaId, $status, $codigoPeca, $motivo);
				if($totalPecas > 0){
					if($peca->pecas){
						$totalPecas = $totalPecas + $peca->total;
					}
					$peca->total = $totalPecas;
				}
				if($totalValor > 0){
					$peca->valor = $peca->valor + $totalValor;
				}
				return($peca);
			}else{
				return(0);
			}
		}
		
		public static function getNumRegEncontrados() {
			global $conexao;

			$pesquisa = 'SELECT FOUND_ROWS() as numRegistros';

			$rs_pesquisa = mysql_query($pesquisa, $conexao) or die(mysql_error());

			if ($row = mysql_fetch_assoc($rs_pesquisa)) {
				return($row['numRegistros']);
			}
			else {
				return(0);
			}
		}
	}
?>
