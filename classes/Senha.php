<?php
	require_once('config.php');
	class Senha{
		
		public static function recuperaSenha($email = ''){
			if(Senha::verificaEmail($email)){
				$senha = Senha::geraSenha($email);
				require_once('Email.php');
				if(Senha::modificaSenha($email, $senha)){
					//echo $senha; exit; //exibir senha sem enviar via email
					if(Email::enviaSenha($email, $senha)){
						return(true);
					}else{
						return(-3);
					}
				}else{
					return(-2);
				}
			}else{
				return(-1);
			}
		}
		
		private static function verificaEmail($email){
			global $conexao;
			
			$busca = 'SELECT email FROM pessoa WHERE email = "'.$email.'" ';
			$rs_busca = mysql_query($busca, $conexao);
			$row_busca = mysql_fetch_assoc($rs_busca);
			if($row_busca){
				return(true);
			}else{
				return(false);
			}
		}
		
		private static function geraSenha($email){
			$novaSenha = substr(sha1($email.date('Hs')),2,8);
			//echo $novaSenha;
			return($novaSenha);
		}
		
		private static function modificaSenha($email = '', $senha = ''){
			global $conexao;
			
			$senhaCript = Senha::criptogradaSenha($senha);
			
			$update = 'UPDATE pessoa SET senha = "'.$senhaCript.'" WHERE email ="'.$email.'" ';
			$rs_update = mysql_query($update, $conexao);
			if($rs_update){
				return(true);
			}else{
				return(false);
			}
		}
		
		public static function criptogradaSenha($senha = ''){
			return(sha1($senha));
		}
		
	}
?>