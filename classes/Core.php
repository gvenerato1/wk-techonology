<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Core
 *
 * @author godutra
 */
class Core {
    public static function getSession($attribute, $defaultValue = '') {
        $ret = '';

        if (array_key_exists($attribute, $_SESSION)) {
            $ret = $_SESSION[$attribute];
        }
        else {
            $ret = $defaultValue;
        }

        return($ret);
    }
	
	public static function getMediaFile($pasta, $id, $arquivo, $tipo, $largura, $altura, $link = '', $titulo) {
		if($tipo == 1){ // imagem
			$img = '';
			if(!empty($link)){
				$img .= '<a href="'.$link.'" target="_blank">';
			}
			$img .= '<img src="media/'.$pasta.'/'.$id.'/'.$arquivo.'" width="'.$largura.'" height="'.$altura.'" alt="'.$titulo.'" title="'.$titulo.'" />';
			if(!empty($link)){
				$img .= '</a>';
			}
			return($img);
		}elseif($tipo == 2){ // flash
			return('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="'.$largura.'" height="'.$altura.'"><param name="movie" id="movie" value="media/'.$pasta.'/'.$id.'/'.$arquivo.'"><embed id="movie" name="movie" src="media/'.$pasta.'/'.$id.'/'.$arquivo.'" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="'.$largura.'" height="'.$altura.'"></embed> </object>');
		}
		
    }

    public static function getCurrentUrl($includeParams = false) {
        if ($includeParams) {
            $serverParam = 'REQUEST_URI';
        }
        else {
            $serverParam = 'SCRIPT_NAME';
        }

        $pageURL = 'http';
        
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        
        $pageURL .= "://";

        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER[$serverParam];
        } 
        else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER[$serverParam];
        }
        
        return $pageURL;
    }

    //TODO: implementar getUrl para todas as situacoes
    public static function getUrl($resource = '') {
        global $conf;
        
        if (array_key_exists('url', $conf)) {
            $url = $conf['url'];
        }
        else {
            $url = '.';
            if ($resource != '' && !file_exists($url . '/' . $resource)) {
                $url = '..';
            }
        }

        return($url . '/' . $resource);
    }

    public static function getSysPath($filePath) {
        $ret = str_replace('//','/',dirname(__FILE__).'/../') . $filePath;

        return($ret);
    }

    public static function getPost($var, $default = '', $removeTags = true, $removeBlank = true) {
        $ret = '';

        if (array_key_exists($var, $_POST)) {
            $ret = $_POST[$var];
            if ($removeTags) {
                $ret = strip_tags($ret);
            }
            if ($removeBlank) {
                $ret = trim($ret);
            }
        }

        if ($ret == '') {
            $ret = $default;
        }

        return($ret);
    }

    public static function getRequest($var, $default = '', $removeTags = true, $removeBlank = true) {
        $ret = '';

        if (array_key_exists($var, $_REQUEST)) {
            $ret = $_REQUEST[$var];

            if ($removeTags) {
                $ret = strip_tags($ret);
            }
            if ($removeBlank) {
                $ret = trim($ret);
            }
        }

        if ($ret == '') {
            $ret = $default;
        }

        return($ret);
    }

    public static function get_include_contents($filename) {
        $contents = '';

        if (is_file($filename)) {
            ob_start();
            include $filename;
            $contents = ob_get_contents();
            ob_end_clean();
        }

        return $contents;
    }

    public static function getTheme() {
        global $conf;

        @session_start();

        $tema = 'padrao';

        if (array_key_exists('tema', $_REQUEST)) {
            $_SESSION['tema'] = $_REQUEST['tema'];
        }
        else if (!array_key_exists('tema', $_SESSION) && array_key_exists('tema', $conf)) { //somente le o tema na configuracao, se nao estiver registrado na sessao atual
            $_SESSION['tema'] = $conf['tema'];
        }

        if (array_key_exists('tema', $_SESSION)) {
            $tema = $_SESSION['tema'];
        }

        return($tema);
    }
	
	public static function getTitle(){
		$fullUrl = $_SERVER['REQUEST_URI'];
		$url = explode('/', $fullUrl);
		
		$title = '';
		
		if(empty($url[2])){
			$title = '';
		}elseif($url[2] == 'quem-somos'){
			$title = 'Quem Somos - ';
		}elseif($url[2] == 'escritorio'){
			$title = 'Escritório - ';
		}elseif($url[2] == 'area-atuacao'){
			$title = 'Área de Atuação - ';
		}elseif($url[2] == 'servidores-publicos'){
			$title = 'Servidores Públicos - ';
		}elseif($url[2] == 'direito-previdenciario'){
			$title = 'Direito Previdenciário - ';
		}elseif($url[2] == 'direito-trabalhista'){
			$title = 'Direito Trabalhista - ';
		}elseif($url[2] == 'direito-administrativo'){
			$title = 'Direito Administrativo - ';
		}elseif($url[2] == 'direito-civil'){
			$title = 'Direito Civil - ';
		}elseif($url[2] == 'contato'){
			$title = 'Contato - ';
		}elseif($url[2] == 'noticias'){
			if(empty($url[3])){
				$title = 'Notícias - ';
			}elseif($url[3] == 'categoria'){
				$categoriaView = NoticiaDAO::getCategoriaByUrl($url[4]);
				$title = $categoriaView->titulo . ' - ';
			}elseif($url[3] == 'view'){
				$noticiaView = NoticiaDAO::getNoticiaByUrl($url[4]); 
				$title = $noticiaView->titulo . ' - ';
			}
		}
		
		return $title;
	}
	
	public static function _bot_detected(){
	  return (
		isset($_SERVER['HTTP_USER_AGENT'])
		&& preg_match('/bot|crawl|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT'])
	  );
	}
}
?>
