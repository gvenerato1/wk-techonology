<?php
error_reporting (E_ALL & ~ E_NOTICE & ~ E_DEPRECATED);
require_once(dirname(__FILE__) . '/../classes/config.php');
require_once(dirname(__FILE__) . '/../classes/injecao.php');
require_once(dirname(__FILE__) . '/../classes/Senha.php');
require_once(dirname(__FILE__) . '/../mod/grupos/classes/GrupoDAO.php');
@session_start();
class Login {
	// member declaration
    public $idUsuario = 0;
    public $nomeUsuario = '';
	public $admin = false;

	public static function autentica($login, $senha) {
        global $conexao;
		
		@session_start();
        
        $login = limpaSQL($login);
		$senha = Senha::criptogradaSenha(limpaSQL($senha));
		
		$id = 0;
		
		if ($login != '' && $senha != '') {
			
			$pesquisa = "SELECT pessoa.id, pessoa.nome, pessoa.login FROM pessoa, pessoaGrupo, grupo ";
			$pesquisa .= "WHERE pessoa.login = '$login' ";
			$pesquisa .= "AND senha = '$senha' ";
			$pesquisa .= "AND pessoa.status = 1 ";
			$pesquisa .= "AND pessoa.id = pessoaGrupo.idPessoa ";
			$pesquisa .= "AND grupo.id = pessoaGrupo.idGrupo";
			//echo $pesquisa;exit;
			$rs_pesquisa = mysql_query($pesquisa, $conexao) or die(mysql_error());
			
			$row_rs_pesquisa = mysql_fetch_assoc($rs_pesquisa);		
			if ($row_rs_pesquisa) {
				Login::setAcesso($row_rs_pesquisa['id']);
				$_SESSION["login_date"] = date('Y-m-d H:i:s');
				$id = $row_rs_pesquisa['id'];
				$nome = $row_rs_pesquisa['nome'];
				$login = $row_rs_pesquisa['login'];
				$pais = $row_rs_pesquisa['pais'];
				
				$_SESSION["usuario_id"] = $id;
				$_SESSION["usuario_nome"] = $nome;
				$_SESSION["usuario_login"] = $login;
				$_SESSION["usuario_pais"] = $pais;

				$grupo = GrupoDAO::getGrupoByIdUsuario($id);
				if($grupo->codigo == 'administrador'){
					$_SESSION["admin"] = true;
				}elseif($grupo->codigo == 'enderecos'){
					$_SESSION["enderecos"] = true;
				}elseif($grupo->id == 'controle'){
					$_SESSION["controle"] = true;
				}elseif($grupo->id == 'vendas'){
					$_SESSION["vendas"] = true;
				}
			} else {
				self::logout();
			}
			
			return($id);
		}
	}
	
	public static function logout() {
        @session_start();
		global $conexao;

        unset($_SESSION['usuario_id']);
		unset($_SESSION['usuario_nome']);
		unset($_SESSION['usuario_login']);
		unset($_SESSION['admin']);
		unset($_SESSION['cadastro']);
		unset($_SESSION['impressao']);
		unset($_SESSION['fiscal']);
        unset($_SESSION["usuario_idGrupo"]);
    }
	
	public static function logoutRedirect() {
		self::logout();
		echo "<meta http-equiv='refresh' content='0;URL=../sistema/'>";
		exit;
	}
	
	public static function isLogado() {
        @session_start();

		if (array_key_exists('usuario_id', $_SESSION)) {
			if ($_SESSION['usuario_id'] > 0) {
				return(true);
			}
			else {
				return(false);
			}
		}
		else {
			return(false);
		}
	}
	
	public static function isAdmin() {
		if (array_key_exists('admin', $_SESSION)) {
			return($_SESSION['admin']);
		}
		else {
			return(false);
		}
	}
	
	public static function isEnderecos() {
		if (array_key_exists('enderecos', $_SESSION)) {
			return($_SESSION['enderecos']);
		}
		else {
			return(false);
		}
	}
	
	public static function isCadastro() {
		if (array_key_exists('cadastro', $_SESSION)) {
			return($_SESSION['cadastro']);
		}
		else {
			return(false);
		}
	}
	
	public static function isVendas() {
		if (array_key_exists('vendas', $_SESSION)) {
			return($_SESSION['vendas']);
		}
		else {
			return(false);
		}
	}
	
	public static function isImpressao() {
		if (array_key_exists('impressao', $_SESSION)) {
			return($_SESSION['impressao']);
		}
		else {
			return(false);
		}
	}
	
	public static function isFiscal() {
		if (array_key_exists('fiscal', $_SESSION)) {
			return($_SESSION['fiscal']);
		}
		else {
			return(false);
		}
	}
	
	public static function isUsuario() {
		if (array_key_exists('usuario', $_SESSION)) {
			return($_SESSION['usuario']);
		}
		else {
			return(false);
		}
	}
	
	public static function getIdUsuario() {
		if (array_key_exists('usuario_id', $_SESSION)) {
			return($_SESSION['usuario_id']);
		}
		else {
			return(-1);
		}
	}

	public static function getIdGrupoUsuario() {
		if (array_key_exists('usuario_idGrupo', $_SESSION)) {
			return($_SESSION['usuario_idGrupo']);
		}
		else {
			return(-1);
		}
	}
	
	public static function getNomeUsuario() {
		if (array_key_exists('usuario_nome', $_SESSION)) {
			return($_SESSION['usuario_nome']);
		}
		else {
			return('');
		}
	}
	
	public static function getLoginUsuario() {
		if (array_key_exists('usuario_login', $_SESSION)) {
			return($_SESSION['usuario_login']);
		}
		else {
			return('');
		}
	}
	
	public static function getPaisUsuario() {
		if (array_key_exists('usuario_pais', $_SESSION)) {
			return($_SESSION['usuario_pais']);
		}
		else {
			return('');
		}
	}
	
	public static function setAcesso($idPessoa = 0){
		global $conexao;
		
		$data = date('Y-m-d');
		$hora = date('H:i:s');
		
		$insert = 'INSERT INTO login ';
		$insert .= '(';
		$insert .= 'idPessoa, ';
		$insert .= 'data, ';
		$insert .= 'hora';
		$insert .= ')VALUES(';
		$insert .= $idPessoa . ', ';
		$insert .= '"' . $data . '", ';
		$insert .= '"' . $hora . '"';
		$insert .= ')';
		//echo $insert; exit;
		
		$rs_insert = mysql_query($insert, $conexao) or die(mysql_error());
		if($rs_insert){
			return(mysql_insert_id($conexao));
		}else{
			return(-1);
		}
	}
	
	public static function getAcesso($campo = ''){
		global $conexao;
		
		$busca = 'SELECT ' . $campo . ' FROM login ORDER BY id DESC LIMIT 1,1';
		$rs_busca = mysql_query($busca, $conexao) or die(mysql_error());
		while($row = mysql_fetch_assoc($rs_busca)){
			return($row[$campo]);
		}
	}
}
?>