<?php
class Valor {
	/**
	* Converte o valor em formato sql para o formato brasileiro
	* @param String $valor
	* @return String
	* @desc Converte a data em formato sql para o formato brasileiro
	*/
	public static function sqlToBr($valor) {
		$ret = '';
		$valor = number_format($valor, 2);
		$result = str_replace('.', ',', $valor);
		$ret = 'R$'.$result;
		
		return($ret);
	}
	
	public static function sqlToBrNew($valor) {
		$ret = '';
		$valor = number_format($valor, 2, ',', '.');
		$ret = 'R$'.$valor;
		
		return($ret);
	}
	
	public static function sqlToBrInput($valor) {
		$ret = '';
		if(!empty($valor)){
			$valor = number_format($valor, 2);
			$result = str_replace('.', ',', $valor);
			$ret = $result;
		}
		
		return($ret);
	}
	
	public static function getResto($valor){
		return($valor % 2);
	}
	
	public static function brToSql($valor){
		$ret = '';
		$result = str_replace(',', '.', $valor);
		$ret = $result;
		
		return($ret);
	}
	
	public static function brToSqlNew($valor) {
		$verificaPonto = ".";
		if(strpos("[".$valor."]", "$verificaPonto")):
			$valor = str_replace('.','', $valor);
			$valor = str_replace(',','.', $valor);
		else:
			$valor = str_replace(',','.', $valor);   
		endif;

	   return $valor;
	}
}
?>