<?php
class Data {
	/**
	* Converte a data em formato brasileiro para o formato usado nas instruções SQL
	* @param String $data
	* @return String
	* @desc Converte a data em formato brasileiro para o formato usado nas instru��es SQL
	*/
	public static function brToSql($data) {
		$ret = '';
		
		$d = explode(' ', $data);
		
		if (!empty($d[0])) {
			list($dia, $mes, $ano) = @split('[/.-]', $d[0]);
			
			if (strlen($dia) == 4) { //se o primeiro elemento tem 4 digitos entao ja e o ano, nao se deve fazer nada
                $ret = $d[0];
            }
            else {
                $ret = $ano . '-' . $mes . '-' . $dia;
            }
		}
		if(!empty($d[1])){
			$ret .= ' ' . $d[1];
		}
		
		return($ret);
	}
	
	/**
	* Converte a data em formato usado nas instru��es SQL para o formato brasileiro 
	* @param String $data
	* @return String
	* @desc Converte a data em formato usado nas instru��es SQL para o formato brasileiro 
	*/
	public static function sqlToBr($data) {
		$ret = '';
		
		$d = explode(' ', $data);
		
        if (!empty($d[0]) && strtotime($d[0])) {
			$ret = date('d/m/Y', strtotime($d[0]));		
		}
		
		if(!empty($d[1]) && $d[1] != '00:00:00'){
			$ret .= ' ' . $d[1];
		}
		
		return($ret);
	}
	
	public static function converteMes($mes){
		if($mes == 1){ $m = 'Janeiro'; }
		if($mes == 2){ $m = 'Fevereiro'; }
		if($mes == 3){ $m = 'Março'; }
		if($mes == 4){ $m = 'Abril'; }
		if($mes == 5){ $m = 'Maio'; }
		if($mes == 6){ $m = 'Junho'; }
		if($mes == 7){ $m = 'Julho'; }
		if($mes == 8){ $m = 'Agosto'; }
		if($mes == 9){ $m = 'Setembro'; }
		if($mes == 10){ $m = 'Outubro'; }
		if($mes == 11){ $m = 'Novembro'; }
		if($mes == 12){ $m = 'Dezembro'; }
		
		return($m);
	}
	
	public static function getWeekday($date) {
		$number = date('w', strtotime($date));
		
		switch ($number) {
			case 0:
				$semana = 'Domingo';
				break;
			case 1:
				$semana = 'Segunda-Feira';
				break;
			case 2:
				$semana = 'Terça-feira';
				break;
			case 3:
				$semana = 'Quarta-feira';
				break;
			case 4:
				$semana = 'Quinta-feira';
				break;
			case 5:
				$semana = 'Sexta-feira';
				break;
			case 6:
				$semana = 'Sábado';
				break;
		}
		
		return $semana;
	}
	
	public static function validateDate($date, $format = 'Y-m-d H:i:s'){
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
}
?>